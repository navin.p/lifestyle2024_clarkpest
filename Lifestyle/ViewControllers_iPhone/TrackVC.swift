//
//  TrackVC.swift
//  Lifestyle
//
//  Created by Rakesh Jain on 14/06/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import MapKit
class TrackVC: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapViewTrack: MKMapView!
    @IBOutlet weak var imgviewTechProfile: UIImageView!
    @IBOutlet weak var lblCompanyName: UILabel!    
    @IBOutlet weak var lblTechName: UILabel!
    
    @IBOutlet weak var btnPhoneNumber: UIButton!
    
    var locationManager : CLLocationManager!
    var dictWODetails = NSDictionary()
    var timer = Timer()
    
    // MARK: View's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        lblCompanyName.text = "\(dictWODetails.value(forKey: "CompanyKey")!)"
        lblTechName.text = "\(dictWODetails.value(forKey: "TechnicianName")!)"
        
        btnPhoneNumber.setTitle("\(checkNullValue(str: "\(dictWODetails.value(forKey: "PrimaryPhone")!)")!)", for: .normal)
        
        if(btnPhoneNumber.currentTitle?.count == 0)
        {
            btnPhoneNumber.setTitle(" ", for: .normal)
        }
        
        // adding marker on technician's starting location
        
        if let lat = (dictWODetails["OnMyWayLatitude"] as? NSString)?.doubleValue {
            
            if let long = (dictWODetails["OnMyWayLongitude"] as? NSString)?.doubleValue
            {
                addMarkerOnMap(lat: lat, long: long)
            }
        }
        
        
        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let loca = locationManager.location?.coordinate
        
        let region = MKCoordinateRegion(center: loca!, latitudinalMeters: 100, longitudinalMeters: 100)
        let adjustedRegion = mapViewTrack.regionThatFits(region)
        
        mapViewTrack.setRegion(adjustedRegion, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    // MARK: WSeb Service
    @objc func updateLocation(){
        
        let empNo = "\(dictWODetails.value(forKey: "EmployeeNo")!)"
        let compKey = "\(dictWODetails.value(forKey: "CompanyKey")!)"
        
        WebService.callAPIBYGET(parameter: NSDictionary(), responseType: "", url: "http://api.lifestyle2024.com/api/Customer/GetEmployeeOnTheWayLocation?EmployeeNo=\(empNo)&companyKey=\(compKey)") { (response, status) in
            
            if(status == "success")
            {
                print(response)
                // adding marker on technician's updated location
                
                if("\(response.value(forKey: "Latitude")!)".count > 0 && "\(response.value(forKey: "Longitude")!)".count > 0)
                {
                    let lat = Double("\(response.value(forKey: "Latitude")!)") ?? 0.0
                    
                    let long = Double("\(response.value(forKey: "Longitude")!)") ?? 0.0
                    
                    if(lat > 0.0 && long > 0.0)
                    {
                        self.addMarkerOnMap(lat: lat, long: long)
                    }
                }
            
            
            
        }
    }
}


// MARK: Fit all Pins on map
func fitAllAnnotations()
{
    var zoomRect = MKMapRect.null
    
    if let userLocation = locationManager.location?.coordinate {
        
        print("user location\(userLocation)")
        let annotationPoint = MKMapPoint(locationManager.location!.coordinate)
        let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
        zoomRect = zoomRect.union(pointRect)
    }
    
    for item in mapViewTrack.annotations
    {
        let annotationPoint = MKMapPoint(item.coordinate)
        let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
        
        zoomRect = zoomRect.union(pointRect)
    }
    
    mapViewTrack.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100.0, left: 100.0, bottom: 100.0, right: 100.0), animated: true)
}

func addMarkerOnMap(lat:Double, long:Double)
{
    let techLoc = MKPointAnnotation()
    techLoc.coordinate = CLLocationCoordinate2D(latitude: lat , longitude: long )
    
    self.mapViewTrack.addAnnotation(techLoc)
}

//MARK: Location Manager Delegate

//    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        let location = locations.last as! CLLocation
//        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//        var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
//        region.center = mapViewTrack.userLocation.coordinate
//        mapViewTrack.setRegion(region, animated: true)
//    }



// MARK: Location access Method
func requestLocationAccess()
{
    let status = CLLocationManager.authorizationStatus()
    switch status
    {
    case .authorizedAlways:
        return
        
    case .denied, .restricted:
        print("location access denied")
        
    default:
        locationManager.requestWhenInUseAuthorization()
    }
}


// MARK: UIButton action
@IBAction func actionOnBack(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
}


@IBAction func actionOnPhoneNumber(_ sender: UIButton) {
    
    if let url = URL(string: "tel://\(dictWODetails.value(forKey: "PrimaryPhone")!)") {
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}


// MARK: MapView Delegate

func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    
    if annotation is MKUserLocation {
        let pin = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        pin.pinTintColor = UIColor.purple
        return pin
        
    } else {
        // handle other annotations
        
    }
    return nil
}
}

