//
//  ContactUSVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/31/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import MessageUI

class ContactUSVC: UIViewController  , MFMailComposeViewControllerDelegate{
    
    
    //MARK:
       //MARK : IBOutlet's
       @IBOutlet weak var lblAddress: UILabel!
    
    //MARK:
       //MARK : Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblAddress.text = "5822 Roseville Rd\nSacramento, CA 95842"
      
    }
    

    //MARK:
       //MARK : IBAction's
       @IBAction func actionOnBack(_ sender: UIButton) {
           
           UIButton.animate(withDuration: 0.2,
                            animations: {
                               sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
           },
                            completion: { finish in
                               UIButton.animate(withDuration: 0.2, animations: {
                                   sender.transform = CGAffineTransform.identity
                                   self.navigationController?.popViewController(animated: true)
                               })
           })
       }
       
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnCalling(_ sender: UIButton) {
        
        let strNumber = "\(String(describing: sender.titleLabel!.text!))"
        
        if(callingFunction(number: strNumber as NSString)){
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
        
    }
    @IBAction func actionOnEmail(_ sender: UIButton) {
        let strEmail = "\(String(describing: sender.titleLabel!.text!))"

        
         if MFMailComposeViewController.canSendMail() {
                 let mail = MFMailComposeViewController()
                 mail.mailComposeDelegate = self
                 mail.setToRecipients(["\(strEmail)"])
                 mail.setMessageBody("", isHTML: true)

                 present(mail, animated: true)
             } else {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)

             }
      }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
          dismiss(animated: true, completion: nil)
      }
}
