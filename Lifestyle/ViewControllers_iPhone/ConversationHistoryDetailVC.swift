//
//  ConversationHistoryDetailVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/31/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class ConversationHistoryDetailVC: UIViewController {
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var lblTicketNumber: UILabel!
       @IBOutlet weak var lblDate: UILabel!
       @IBOutlet weak var lblStatus: UILabel!
       @IBOutlet weak var lblDetail: UILabel!
    
    var aryList = NSMutableArray()
    var dictDetail = NSDictionary()

    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvList.tableFooterView = UIView()
        lblTicketNumber.text = "\(dictDetail.value(forKey: "ticketNumber")!)"
                lblDate.text = "\(dictDetail.value(forKey: "date")!)"
                lblStatus.text = "\(dictDetail.value(forKey: "status")!)"
                lblDetail.text = "\(dictDetail.value(forKey: "detail")!)"
        
        
        if(dictDetail.value(forKey: "id")as! Int != 1){
            var dict = NSMutableDictionary()
                   dict = NSMutableDictionary()
                   dict.setValue("Clark Pest Control prides itself on exceeding customer expectations in an ethical, professional, responsive and caring manner. The manager has agreed to issue a refund for all pest services that were completed and paid for. The customer did not previously inform us of any issues with the termite work that was completed. A refund for these services will not be issued. We apologize for any inconvenience that this may have caused and hope that Ms. ***** will consider Clark for any future pest control needs.", forKey: "message")
                   dict.setValue("08/15/2019 03:34 PM", forKey: "time")
                   dict.setValue(1, forKey: "id")
                   dict.setValue("John Smith - Clark Pest Team", forKey: "messageBy")

                   aryList.add(dict)
                   
                   dict = NSMutableDictionary()
                   dict.setValue("I have reviewed the response made by the business in reference to my concern, and find that this resolution is satisfactory to me.", forKey: "message")
                   dict.setValue("08/16/2019 05:00 PM", forKey: "time")
                   dict.setValue(2, forKey: "id")
                   dict.setValue("John Smith - Clark Pest Team", forKey: "messageBy")

                   aryList.add(dict)
            
            
            
        }
        if(aryList.count != 0){
            self.tvList.scrollToBottom()
        }
           
    }
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    
    @IBAction func actionOnSend(_ sender: UIButton) {
           var dict = NSMutableDictionary()
                             dict = NSMutableDictionary()
        dict.setValue("\(txtMessage.text!)", forKey: "message")
                             dict.setValue("08/16/2019 05:10 PM", forKey: "time")
                             dict.setValue(4, forKey: "id")
        dict.setValue("John Smith - Clark Pest Team", forKey: "messageBy")

                             aryList.add(dict)
        self.tvList.reloadData()
        if(aryList.count != 0){
                   self.tvList.scrollToBottom()
               }
        txtMessage.text = ""
        
       // self.view.endEditing(true)
       }
       
}
//MARK:
//MARK: UITableViewDelegate
extension ConversationHistoryDetailVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        
     /*   if(indexPath.section == 0){
            let cell = tvList.dequeueReusableCell(withIdentifier: "ConversationHistoryDetailCell", for: indexPath as IndexPath) as! ConversationHistoryDetailCell

            cell.lblTicketNumber.text = "\(dictDetail.value(forKey: "ticketNumber")!)"
            cell.lblDate.text = "\(dictDetail.value(forKey: "date")!)"
            cell.lblStatus.text = "\(dictDetail.value(forKey: "status")!)"
            cell.lblDetail.text = "\(dictDetail.value(forKey: "detail")!)"
            return cell

        }else{*/
            let dictData = aryList.object(at: indexPath.row)as! NSDictionary
            let msg =  "\(dictData.value(forKey: "message")!)"
            let time =  "\(dictData.value(forKey: "time")!)"
             let messageBy =  "\(dictData.value(forKey: "messageBy")!)"

            if(indexPath.row % 2 != 0){
                
                let cell = tvList.dequeueReusableCell(withIdentifier: "Sender", for: indexPath as IndexPath) as! ConversationHistorySenderCell

                cell.lblSenderDate.text = time
                cell.lblSenderMSG.text = "\(msg)"
                cell.lblSenderMsgBy.text = ""
                cell.viewSender.layer.cornerRadius = 8.0
                cell.viewSender.layer.masksToBounds = true
                return cell

            }else{
                let cell = tvList.dequeueReusableCell(withIdentifier: "Reciever", for: indexPath as IndexPath) as! ConversationHistoryReciverCell
                
                cell.lblReviverDate.text = time
                cell.lblReciverMsg.text = "\(msg)"
                cell.lblReciverMsgBy.text = messageBy

                cell.viewReciver.layer.cornerRadius = 8.0
                cell.viewReciver.layer.masksToBounds = true
                return cell

            }
      //  }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //return section == 0 ? 0 : 40
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = UIColor.groupTableViewBackground
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Conversation"
        headerView.addSubview(label)
        return headerView
    }
}
// MARK: - ----------------UITextViewDelegate
// MARK: -
extension ConversationHistoryDetailVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "All", limitValue: 999999999999999)
        
    }
}




//MARK:
//MARK: UITableViewCell
class ConversationHistoryDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblTicketNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    
}
class ConversationHistorySenderCell: UITableViewCell {
    @IBOutlet weak var lblSenderMSG: UILabel!
       @IBOutlet weak var lblSenderDate: UILabel!
    @IBOutlet weak var viewSender: UIView!
    @IBOutlet weak var lblSenderMsgBy: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
        // Configure the view for the selected state
    }
    
    
}
class ConversationHistoryReciverCell: UITableViewCell {
    @IBOutlet weak var lblReciverMsg: UILabel!
       @IBOutlet weak var lblReciverMsgBy: UILabel!

       @IBOutlet weak var lblReviverDate: UILabel!
       @IBOutlet weak var viewReciver: UIView!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        
        // Configure the view for the selected state
    }
    
    
}
extension UITableView {

    func scrollToBottom(){

        DispatchQueue.main.async {
                   let indexPath = IndexPath(
                       row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                       section: self.numberOfSections - 1)
                   self.scrollToRow(at: indexPath, at: .bottom, animated: true)
               }
    }

    func scrollToTop() {

        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}
