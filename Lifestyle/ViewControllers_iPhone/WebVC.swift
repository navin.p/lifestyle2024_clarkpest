//
//  WebVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 20/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import WebKit
class WebVC: UIViewController,WKNavigationDelegate {
   
    // outlets
    @IBOutlet weak var webkit: WKWebView!
    
    // variables
    var strWebURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(isInternetAvailable()){
            
            FTIndicator.showProgress(withMessage: "Loading...", userInteractionEnable: false)
            let url = URL(string: strWebURL)!
            webkit.load(URLRequest(url: url))
            webkit.allowsBackForwardNavigationGestures = true
            webkit.navigationDelegate = self
        }
        else{
             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
       
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        FTIndicator.dismissProgress()
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
