//
//  WODocumentsVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 02/04/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import SafariServices

class CellWODocuments: UITableViewCell{
    
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var btnTitle: UIButton!
}
class WODocumentsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SFSafariViewControllerDelegate {

    // outlet
    @IBOutlet weak var tblviewWODocuments: UITableView!
    
    // varialbe
    var dictWO = NSDictionary()
    var arrayDocs = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblviewWODocuments.tableFooterView = UIView()
        if(isInternetAvailable()){
            getWorkOrderDocuments()
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    
   // MARK: Web Service
   func getWorkOrderDocuments(){
    
     let strURL = BaseURL+"GetWorkOrderDocuments?companyKey=\(dictWO.value(forKey: "CompanyKey")!)&workorderNo=\(dictWO.value(forKey: "WorkOrderNo")!)&AppKey=\(dictWO.value(forKey: "AppKey")!)"   
 
//    FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)

    customDotLoaderShowOnFull(message: "", controller: self)
    
    WebService.callAPIBYGET(parameter: NSDictionary(), responseType: "", url: strURL) { (response, status) in
        
//        FTIndicator.dismissProgress()
        customeDotLoaderRemove()
        if(status == "success"){
            
            print(response)
            if(containsKey(dict: response))
            {
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: response.value(forKey: "Message") as! String, viewcontrol: self)
            }
            else
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.arrayDocs = response.value(forKey: "data") as! NSArray
                    self.tblviewWODocuments.reloadData()
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: "No document found", viewcontrol: self)
                }
            }
           
        }
        else {
             showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    }
    
    // MARK: UITableView delegate and data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDocs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellWODocs = tableView.dequeueReusableCell(withIdentifier: "CellWODocuments", for: indexPath as IndexPath) as! CellWODocuments
        let dict = arrayDocs.object(at: indexPath.row) as! NSDictionary
      
        cellWODocs.lblAccNo.text = "Account #:\(dict.value(forKey: "AccountNo")!)"
      
        cellWODocs.btnTitle.tag = indexPath.row
        
        cellWODocs.btnTitle.addTarget(self, action: #selector(actionOnTitle), for: .touchUpInside)
        
        cellWODocs.btnTitle.setTitle("\(dict.value(forKey: "Title")!)", for: .normal)
      
        return cellWODocs
    }
    
    // MARK: UIButton action
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func actionOnTitle(sender:UIButton)
    {
        let dict = arrayDocs.object(at: sender.tag) as! NSDictionary
        
        let signLink = "\(dict.value(forKey: "Path")!)"
        
        let url = NSURL(string: signLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
        
        if(url != nil)
        {
            let safariVC = SFSafariViewController(url: url!)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
        }
    }    
   
    
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DocumentsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TransactionsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnPastSercices(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is WorkOrdersVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TaskVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    // MARK: Safari controller delegate
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
        controller.dismiss(animated: true, completion: nil)
        
    }
}
