//
//  ConversationHistoryVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/31/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class ConversationHistoryVC: UIViewController {
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var tvList: UITableView!
   
    
    var aryList = NSMutableArray()
    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        tvList.tableFooterView = UIView()
        
        var dict = NSMutableDictionary()
        dict = NSMutableDictionary()
        dict.setValue("2101", forKey: "ticketNumber")
        dict.setValue("Open", forKey: "status")
        dict.setValue("12/30/2019 03:34 PM", forKey: "date")
        dict.setValue("A Clark pest control employee came to my house to inspect it for a refi loan when the employee went under my house he knocked loose a heater vent pipe so I was unable to use my heater When I noticed that the heater was not working properly, I called the employee said that who would come back to fix the damage but he never did. Please send someone to take care of this.", forKey: "detail")
        dict.setValue(1, forKey: "id")
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("1245", forKey: "ticketNumber")
        dict.setValue("Resolved", forKey: "status")
        dict.setValue("08/15/2019 06:36 PM", forKey: "date")
        dict.setValue("I am filing a report because I got a worse service from the clark pest control company. They charged me $1290 in the beginning and I signed a contract for $45 a month. But the ants issue was never resolved I called their office and tried to schedule when I would be home to show them the problem but everytime I schedule they just show up when the want knowing I'm not Available. ", forKey: "detail")
        dict.setValue(2, forKey: "id")
        aryList.add(dict)
      
    }
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
}
//MARK:
//MARK: UITableViewDelegate
extension ConversationHistoryVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "ConversationHistoryCell", for: indexPath as IndexPath) as! ConversationHistoryCell
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblTicketNumber.text = "\(dictData.value(forKey: "ticketNumber")!)"
        cell.lblDate.text = "\(dictData.value(forKey: "date")!)"
        cell.lblStatus.text = "\(dictData.value(forKey: "status")!)"
        cell.lblDetail.text = "\(dictData.value(forKey: "detail")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCVC = storyboard.instantiateViewController(withIdentifier: "ConversationHistoryDetailVC") as! ConversationHistoryDetailVC
        SCVC.dictDetail  = aryList.object(at: indexPath.row)as! NSDictionary
        self.navigationController?.pushViewController(SCVC, animated: true)
    }
}


//MARK:
//MARK: UITableViewCell
class ConversationHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lblTicketNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDetail: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
           self.selectionStyle = .none

           // Configure the view for the selected state
       }
    
    
}
