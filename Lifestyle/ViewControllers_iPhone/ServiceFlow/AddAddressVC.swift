//
//  AddAddressVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/11/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {
    //MARK:
      //MARK : IBOutlet's
   
           @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnDropAddress: UIButton!
    @IBOutlet weak var btnDropState: UIButton!

    @IBOutlet weak var lblAddress: UILabel!

    //MARK:
    //MARK : LifeCycle's
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSave.layer.cornerRadius =  btnSave.frame.size.height/2
      //  btnDropAddress.layer.cornerRadius =  btnDropAddress.frame.size.height/2
       // btnDropState.layer.cornerRadius =  btnDropAddress.frame.size.height/2

        // Do any additional setup after loading the view.
    }
    

   //MARK:
      //MARK : IBAction's
      @IBAction func actionOnBack(_ sender: UIButton) {
          
          UIButton.animate(withDuration: 0.2,
                           animations: {
                              sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
          },
                           completion: { finish in
                              UIButton.animate(withDuration: 0.2, animations: {
                                  sender.transform = CGAffineTransform.identity
                                  self.navigationController?.popViewController(animated: true)
                              })
          })
      }
    @IBAction func actionOnDropDownAddress(_ sender: UIButton) {
           
           let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
           vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
           vc.modalTransitionStyle = .coverVertical
           vc.delegate = self
           vc.strTag = 21
           var dict = NSMutableDictionary()
           let aryList = NSMutableArray()
           
           dict.setValue("11802 Warfield St, San Antonio, TX 78216, United States", forKey: "address")
           
           aryList.add(dict)
           dict = NSMutableDictionary()
           dict.setValue("11815 Warfield St, San Antonio, TX 78216, United States", forKey: "address")
           aryList.add(dict)
           
           dict = NSMutableDictionary()
           dict.setValue("231 E Nakoma Dr, San Antonio, TX 78216, United States", forKey: "address")
           aryList.add(dict)
           
           vc.aryTBL = aryList
           self.present(vc, animated: false, completion: {})
           
       }
}


//MARK:
   //MARK: PopUpDelegate's
extension AddAddressVC: PopUpDelegate {
    func getDataFromPopUpDelegate(dict: NSDictionary, tag: Int) {
      lblAddress.text = "\(dict.value(forKey: "address")!)"
    }
    
}

//MARK:
//MARK: UITextFieldDelegate's
extension AddAddressVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
