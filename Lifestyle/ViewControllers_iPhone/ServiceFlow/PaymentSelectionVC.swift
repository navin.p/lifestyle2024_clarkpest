//
//  PaymentSelectionVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/11/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class PaymentSelectionVC: UIViewController {
   
    //MARK:
      //MARK : IBOutlet's
      @IBOutlet weak var tvList: UITableView!
      @IBOutlet weak var btnAcceptContinue: UIButton!

        var aryList = NSMutableArray()
      var arySelectedList = NSMutableArray()
    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        var dict = NSMutableDictionary()
        dict.setValue("Credit Card", forKey: "title")
        
        aryList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("EFT", forKey: "title")
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("PayPal", forKey: "title")
        aryList.add(dict)
        tvList.tableFooterView = UIView()
        btnAcceptContinue.layer.cornerRadius =  btnAcceptContinue.frame.size.height/2
        arySelectedList.add(aryList.object(at: 0))
    }
    
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
 
    @IBAction func actionOnSubmit(_ sender: UIButton) {
           
           UIButton.animate(withDuration: 0.2,
                            animations: {
                               sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
           },
                            completion: { finish in
                               UIButton.animate(withDuration: 0.2, animations: {
                                   sender.transform = CGAffineTransform.identity
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)

                                let SCVC = storyboard.instantiateViewController(withIdentifier: "PreffredScheduleVC") as! PreffredScheduleVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
               })
           })
       }
    

}
//MARK:
//MARK: UITableViewDelegate
extension PaymentSelectionVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath as IndexPath) as! PaymentCell
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
 
        cell.lblTitle.text = "\(dictData.value(forKey: "title")!)"
       
        if(arySelectedList.contains(aryList.object(at: indexPath.row))){
            cell.viewRadio.backgroundColor = appThemeColor
              }else{
            cell.viewRadio.backgroundColor = UIColor.groupTableViewBackground
              }
             return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        arySelectedList = NSMutableArray()
        arySelectedList.add(aryList.object(at: indexPath.row))
        self.tvList.reloadData()
    }
    
}


//MARK:
//MARK: UITableViewCell
class PaymentCell: UITableViewCell {
   
  @IBOutlet weak var viewRadio: UIView!
    @IBOutlet weak var lblTitle: UILabel!

}
