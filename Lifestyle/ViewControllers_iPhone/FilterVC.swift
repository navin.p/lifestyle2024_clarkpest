//
//  FilterVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 04/04/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {
       // outlets
    
    @IBOutlet weak var btnSelectStatus: UIButton!
    @IBOutlet weak var txtfldCompanyName: ACFloatingTextfield!
    @IBOutlet weak var txtfldTechnicianName: ACFloatingTextfield!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
   
    var arrStatus = NSMutableArray()
    var strStatus = ""
    var strFromDate = ""
    var strToDate = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        btnSelectStatus.layer.borderWidth = 1.0
        btnSelectStatus.layer.borderColor = borderColor
        btnSelectStatus.layer.cornerRadius = 2.0
        
        btnFromDate.layer.borderWidth = 1.0
        btnFromDate.layer.borderColor = borderColor
        btnFromDate.layer.cornerRadius = 2.0
        
        btnToDate.layer.borderWidth = 1.0
        btnToDate.layer.borderColor = borderColor
        btnToDate.layer.cornerRadius = 2.0
        
         btnSubmit.layer.cornerRadius =  btnSubmit.frame.size.height/2
        
        arrStatus = ["All","Completed","Reset","InComplete","Cancelled"]
    }

    // MARK: UIButton action
    
   
   
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnSelectStatus(_ sender: UIButton) {
        
        self.view.endEditing(true)
        gotoPopViewWithArray(sender: sender , aryData: arrStatus, strTitle: "")
    }
    
    @IBAction func actionOnFromDate(_ sender: UIButton) {
        self.view.endEditing(true)
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @IBAction func actionOnToDate(_ sender: UIButton) {
        self.view.endEditing(true)
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(txtfldCompanyName.text?.count == 0 && txtfldTechnicianName.text?.count == 0 && strFromDate.count == 0 && strToDate.count == 0 && strStatus.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select atleast one field to filter work orders", viewcontrol: self)
            return
        }
        else if(strFromDate.count > 0 && strToDate.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select To Date", viewcontrol: self)
            return
            
        }
        else if(strFromDate.count == 0 && strToDate.count > 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select From Date", viewcontrol: self)
            return
        }
        else if(strFromDate.count > 0 && strToDate.count > 0)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let fromDate = dateFormatter.date(from:strFromDate)!
            let toDate = dateFormatter.date(from:strToDate)!
            if(fromDate>toDate)
            {
                // "From Date should be less than To Date"
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "From Date should be less than To Date", viewcontrol: self)
                return
            }
        }
       
            
            // pass these data to next controller
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let filteredWOVC = storyboard.instantiateViewController(withIdentifier: "FilteredWorkOrderVC") as! FilteredWorkOrderVC
            filteredWOVC.strCompanyName = (txtfldCompanyName.text?.count)! > 0 ? txtfldCompanyName.text! : ""
            filteredWOVC.strTechnicianName = (txtfldTechnicianName.text?.count)! > 0 ? txtfldTechnicianName.text! : ""
            filteredWOVC.strFromDate = strFromDate
            filteredWOVC.strToDate = strToDate
            filteredWOVC.strStatus = strStatus
           
            self.navigationController?.pushViewController(filteredWOVC, animated: true)        
        
    }
    
    /*    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
     
     UIButton.animate(withDuration: 0.2,
     animations: {
     sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
     },
     completion: { finish in
     UIButton.animate(withDuration: 0.2, animations: {
     sender.transform = CGAffineTransform.identity
     
     var vcFound = false
     for vc in (self.navigationController?.viewControllers)!{
     if(vc is DocumentsVC)
     {
     vcFound = true
     self.navigationController?.popToViewController(vc, animated: false)
     break
     }
     }
     if(vcFound == false)
     {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
     self.navigationController?.pushViewController(docVC, animated: true)
     }
     
     })
     })
     
     
     
     }
     
     @IBAction func actionOnTransactions(_ sender: UIButton) {
     //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
     //        let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
     //        self.navigationController?.pushViewController(docVC, animated: true)
     }
     
     @IBAction func actionOnPastService(_ sender: UIButton) {
     
     UIButton.animate(withDuration: 0.2,
     animations: {
     sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
     },
     completion: { finish in
     UIButton.animate(withDuration: 0.2, animations: {
     sender.transform = CGAffineTransform.identity
     
     var vcFound = false
     for vc in (self.navigationController?.viewControllers)!{
     if(vc is WorkOrdersVC)
     {
     vcFound = true
     self.navigationController?.popToViewController(vc, animated: false)
     break
     }
     }
     if(vcFound == false)
     {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
     self.navigationController?.pushViewController(docVC, animated: true)
     }
     
     })
     })
     
     
     
     
     }
     
     @IBAction func actionOnTask(_ sender: UIButton) {
     
     UIButton.animate(withDuration: 0.2,
     animations: {
     sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
     },
     completion: { finish in
     UIButton.animate(withDuration: 0.2, animations: {
     sender.transform = CGAffineTransform.identity
     
     var vcFound = false
     for vc in (self.navigationController?.viewControllers)!{
     if(vc is TaskVC)
     {
     vcFound = true
     self.navigationController?.popToViewController(vc, animated: false)
     break
     }
     }
     if(vcFound == false)
     {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
     self.navigationController?.pushViewController(docVC, animated: true)
     }
     
     })
     })
     
     
     }*/
    
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is DocumentsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
        
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is TransactionsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
        
        
    }
    
    @IBAction func actionOnPastServices(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is WorkOrdersVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
        
        
    }
    
    @IBAction func actionOnContact(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                              
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                
                            })
        })
        
        
        
        
    }
    
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is TaskVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    // MARK: PopUpView method
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleSelectionOnEditOpportunity = self
        vc.aryTBL = arrStatus
        self.present(vc, animated: false, completion: {})
        
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let vc: DateTimePicker = self.storyboard!.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleOnSelectionOfDate = self
        vc.strType = strType
        vc.minimumDate = false
        self.present(vc, animated: true, completion: {})
        
    }
}

extension FilterVC: datePickerView {
    
    func setDateOnSelction(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        if(tag == 1)
        {
            let strTempDate = dateTimeConvertor(str: strDate, formet: "yyyy-MM-dd", strFormatToBeConverted: "MM/dd/yyyy")
            if(strTempDate.count == 0)
            {
                btnFromDate.setTitle("", for: .normal)
                strFromDate = ""
            }
            else{
                btnFromDate.setTitle(strTempDate, for: .normal)
                strFromDate = strTempDate
            }
        }
        else if(tag == 2)
        {
            let strTempDate = dateTimeConvertor(str: strDate, formet: "yyyy-MM-dd", strFormatToBeConverted: "MM/dd/yyyy")
            if(strTempDate.count == 0)
            {
                btnToDate.setTitle("", for: .normal)
                strToDate = ""
            }
            else{
                btnToDate.setTitle(strTempDate, for: .normal)
                strToDate = strTempDate
            }
        }
    }
}

extension FilterVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(str : String) {
        self.view.endEditing(true)
        btnSelectStatus.setTitle(str, for: .normal)
        strStatus = str
        
    }
}
