//
//  ChangePasswordVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 22/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import FirebaseAuth
class ChangePasswordVC: UIViewController {

    
    @IBOutlet weak var txtfldOldPassword: ACFloatingTextfield!
    @IBOutlet weak var txtfldNewPassword: ACFloatingTextfield!
    @IBOutlet weak var txtfldConfirmPassword: ACFloatingTextfield!
  
    @IBOutlet weak var btnUpdate: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnUpdate.layer.cornerRadius = btnUpdate.frame.size.height/2
    }

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnUpdate(_ sender: UIButton) {
        
        if(txtfldOldPassword.text?.count == 0)
        {
            self.view.endEditing(true)
             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "please enter old password", viewcontrol: self)
        }
        else if((txtfldNewPassword.text?.count)! < 8)
        {
            self.view.endEditing(true)

             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "password should be minimum 8 characters long", viewcontrol: self)
        }
        else if((txtfldConfirmPassword.text?.count)! == 0)
        {
            self.view.endEditing(true)

             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "please enter confirm password", viewcontrol: self)
        }
        else if(txtfldNewPassword.text != txtfldConfirmPassword.text)
        {
            self.view.endEditing(true)

             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "New password and confirm password are not same", viewcontrol: self)
        }
        else if(txtfldNewPassword.text?.isAlphanumeric == false)
        {
          showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "1. Password must be at least 8 characters and at most 30 characters.\n2. Password must have at least one non letter or digit character(!@#$&*).\n3. Password must have at least one digit ('0'-'9').\n4. Password must have at least one uppercase ('A'-'Z').\n5. Password must have at least one lowercase ('a'-'z').", viewcontrol: self)
        }
        else
        {// update password
            
            if(isInternetAvailable()){
               changePassword()
            }
            else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
    }
    
    /*@IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
     var vcFound = false
     for vc in (self.navigationController?.viewControllers)!{
     if(vc is DocumentsVC)
     {
     vcFound = true
     self.navigationController?.popToViewController(vc, animated: false)
     break
     }
     }
     if(vcFound == false)
     {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
     self.navigationController?.pushViewController(docVC, animated: true)
     }
     }
     
     @IBAction func actionOnTransactions(_ sender: UIButton) {
     //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
     //        let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
     //        self.navigationController?.pushViewController(docVC, animated: true)
     }
     
     @IBAction func actionOnPastService(_ sender: UIButton) {
     
     var vcFound = false
     for vc in (self.navigationController?.viewControllers)!{
     if(vc is WorkOrdersVC)
     {
     vcFound = true
     self.navigationController?.popToViewController(vc, animated: false)
     break
     }
     }
     if(vcFound == false)
     {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
     self.navigationController?.pushViewController(docVC, animated: true)
     }
     }
     
     @IBAction func actionOnTask(_ sender: UIButton) {
     
     var vcFound = false
     for vc in (self.navigationController?.viewControllers)!{
     if(vc is TaskVC)
     {
     vcFound = true
     self.navigationController?.popToViewController(vc, animated: false)
     break
     }
     }
     if(vcFound == false)
     {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
     self.navigationController?.pushViewController(docVC, animated: true)
     }
     }*/
    
    @IBAction func actionOnContact(_ sender: UIButton) {
          
          UIButton.animate(withDuration: 0.2,
                           animations: {
                              sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
          },
                           completion: { finish in
                              UIButton.animate(withDuration: 0.2, animations: {
                                  sender.transform = CGAffineTransform.identity
                                
                                      let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                      let docVC = storyboard.instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
                                      self.navigationController?.pushViewController(docVC, animated: true)
                                  
                              })
          })
          
          
          
          
      }
    
    @IBAction func actionOnHome(_ sender: UIButton) {
      
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DocumentsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
            self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TransactionsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
            self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnPastService(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is WorkOrdersVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
            self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TaskVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
            self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
    }
    
    
    @IBAction func actionOnPasswordInfo(_ sender: UIButton) {
        
        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "1. Password must be at least 8 characters and at most 30 characters.\n2. Password must have at least one non letter or digit character(!@#$&*).\n3. Password must have at least one digit ('0'-'9').\n4. Password must have at least one uppercase ('A'-'Z').\n5. Password must have at least one lowercase ('a'-'z').", viewcontrol: self)
    }
    
    func changePassword(){
        
        self.view.endEditing(true)
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
   
        customDotLoaderShowOnFull(message: "", controller: self)
    
        let email = UserDefaults.standard.value(forKey: "email") as! String
        let credential = EmailAuthProvider.credential(withEmail: email, password: txtfldOldPassword.text!)
        Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (error) in
            if error == nil {
                Auth.auth().currentUser!.updatePassword(to: self.txtfldNewPassword.text!) { (errror) in
                    
//                    FTIndicator.dismissProgress()
                    customeDotLoaderRemove()

                    if (error == nil)
                    {
                        UserDefaults.standard.set(self.txtfldNewPassword.text!, forKey: "password")
                        UserDefaults.standard.synchronize()
                        self.txtfldOldPassword.text = nil
                        self.txtfldNewPassword.text = nil
                        self.txtfldConfirmPassword.text = nil
                        
                        let alertController = UIAlertController(title: "Message", message: "Password updated successfully", preferredStyle: .alert)
                        
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                            
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    else{
                         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage:alertSomeError , viewcontrol: self)
                    }
                }
            } else {
               
                FTIndicator.dismissProgress()
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: getFirebaseErrorMessage(errorCode: errCode), viewcontrol: self)
                }
            }
        })
    }
}
