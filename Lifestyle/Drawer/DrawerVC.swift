//
//  ViewBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class DrawerVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var btnTransprant: UIButton!

    var aryList = NSMutableArray()
    var aryImage = NSMutableArray()

    weak var handleDrawerView: DrawerScreenDelegate?

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTransprant.isHidden = true
        aryList = ["Dashboard", "My Tasks", "Transactions", "Documents", "Service","Conversation History", "Change Password", "Logout"]// Work Orders renamed to Services
       aryImage = [dashboard, task, transaction, document, workorder, changepassword, logout]
        
        
       // lblVersion.text = app_Version
        lblVersion.text = ""
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                UIView.animate(withDuration: 0.5) {
                    self.tvlist.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.btnTransprant.isHidden = false

                }
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        handleDrawerView?.refreshDrawerScreen(strType: "", tag: 0)
        UIView.animate(withDuration: 0.5) {
            self.tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DrawerVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0){
            return 1
        }else{
            return aryList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer1", for: indexPath as IndexPath) as! DrawerCell
            cell.imageView?.layer.cornerRadius = 50.0
            cell.imageView?.clipsToBounds = true
            cell.lblName.text = "\(nsud.value(forKey: "email")!)"
            
       /*     if(nsud.value(forKey: "Aahar_LoginData") != nil){
                let dictLoginData = nsud.value(forKey: "Aahar_LoginData")as! NSDictionary
                cell.lblName.text = "\(dictLoginData.value(forKey: "Name")!)"
                cell.profileimage.layer.borderWidth = 1.0
                cell.profileimage.layer.masksToBounds = false
                cell.profileimage.layer.cornerRadius = cell.profileimage.frame.height/2
                cell.profileimage.clipsToBounds = true
                cell.profileimage.layer.borderColor = UIColor.white.cgColor
                let urlImage = "http://aahar.org.in/ProfileImage/\(dictLoginData.value(forKey: "ProfileImage")!)"
                cell.profileimage.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "profile"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    print(url ?? 0)
                }, usingActivityIndicatorStyle: .gray)
            }
            */
          //  let dict = aryList.object(at: indexPath.row)as! NSDictionary
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer2", for: indexPath as IndexPath) as! DrawerCell
            cell.lblTitle.text = "\(aryList[indexPath.row])"
           // cell.imageMenu.image = UIImage(named: "\(aryImage[indexPath.row])")
            
            
//           let dict = aryList.object(at: indexPath.row)as! NSDictionary
//            cell.lblTitle.text = dict["title"]as? String
//            cell.imageMenu.image = UIImage(named: "\(dict["image"]!)")
            return cell
        }
   
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
            let strTemp = aryList.object(at: indexPath.row)as! String
            UIView.animate(withDuration: 0.5) {
                self.tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                self.dismiss(animated: false) {
                    self.handleDrawerView?.refreshDrawerScreen(strType: strTemp, tag: indexPath.row)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class DrawerCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageMenu: UIImageView!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var lblName: UILabel!


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
